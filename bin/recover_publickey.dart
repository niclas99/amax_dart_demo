import 'dart:typed_data';
import 'package:amaxdart/amaxdart.dart';
import 'package:amaxdart/src/serialize.dart' as ser;
import 'package:amaxdart_ecc/amaxdart_ecc.dart' as ecc;

const api = 'https://expnode.amaxscan.io';
const chainId = '2403d6f602a87977f898aa3c62c79a760f458745904a15b3cd63a106f62adc16';

const txid = 'aa235b02c07b4a77b635fc848d0d34cd3cdf2a3d2d2b2bb2540b34b9bb643ac6';


Future<void> main() async {
  AMAXClient client = AMAXClient(api, 'v1');

  // print result
  var trx = await client.getTransaction(txid);
  print(trx);

  var trxData = trx.trx!.receipt!.trx! as List<dynamic>;
  Map trxMap = trxData[1];
  var packedTrx = trxMap["packed_trx"];
  Uint8List serializedTrx = ser.hexToUint8List(packedTrx);

  Uint8List signBuf =
      Uint8List.fromList(List.from(ser.stringToHex(chainId))
        ..addAll(serializedTrx)
        ..addAll(Uint8List(32)));

  var hexData = ser.arrayToHex(signBuf);
  print(hexData);

  var signatures = trxMap["signatures"][0];
  print(signatures);

  var publicKey = ecc.AMAXSignature.fromString(signatures).recover(signBuf);
  print(publicKey);
}
